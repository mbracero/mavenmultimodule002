package org.mbracero.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mbracero.greeter.Greeter;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoader;

@SuppressWarnings("serial")
public class CustomServlet extends HttpServlet {
	
	private static final String NO_PARAM_VALUE = "0";
	
	private static final String ID_PARAM_VALUE = "id";
	
	public void service(HttpServletRequest req, HttpServletResponse res)
		throws IOException, ServletException {
		
		String idParamValue = req.getParameter(ID_PARAM_VALUE);
		
		String numberString = (idParamValue != null && !"".equals(idParamValue) ? idParamValue : NO_PARAM_VALUE);
		
		int number = Integer.parseInt( numberString );
		
		// Hacemos uso del proyecto services
		String msg = this.getGreeter().getRegards(number);
		
		PrintWriter out = res.getWriter();
        out.println( msg );
        out.flush();
        out.close();
	}
	
	private Greeter getGreeter(){
        return (Greeter) this.getApplicationContext().getBean( "greeter" );
    }
    
    private ApplicationContext getApplicationContext() {
        return ContextLoader.getCurrentWebApplicationContext();
    }
}
