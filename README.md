## mavenMultiModule002

Proyecto pruebas con multimodulos en maven. Aniadido proyecto padre (mavenMultiModule002) con dos proyectos.

Existe un proyecto para exponer servicios (services) de tipo jar.

El otro proyecto hijo (master) es proyecto web (war) y usa el proyecto services para consumir servicios.

Se han aniadido dependencias con spring (context y web).

**mavenMultiModule002:**

Generacion de proyecto 'padre'

`mvn archetype:generate`

Cambiamos el paquete (packaging) del pom.xml de jar a pom.

_This page holds a list of archetypes (even not hosted at apache)._

`http://docs.codehaus.org/display/MAVENUSER/Archetypes+List`

**master:**

`mvn archetype:generate -DgroupId=org.mbracero -DartifactId=master -DarchetypeArtifactId=maven-archetype-webapp -DinteractiveMode=false`

**services:**

`mvn archetype:generate -DgroupId=org.mbracero -DartifactId=services -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false`


Aniadimos el tipo de empaquetado:

`<packaging>jar</packaging>`

Y el nombre para la generacion del build:

```
<build>
	<finalName>${artifactId}-${version}</finalName>
</build>
```

Construimos el proyecto:

`mvn clean install`

Para probarlo inicializamos el jetty:

`mvn -pl master/ jetty:run`

Y en el navegador:

`http://localhost:9091/master/customservleturl`

`http://localhost:9091/master/customservleturl?id=3`

