package org.mbracero.greeter;

public interface Greeter {
	String getRegards(final int number);
}
