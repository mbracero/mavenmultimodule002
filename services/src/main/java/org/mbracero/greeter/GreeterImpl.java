package org.mbracero.greeter;

public class GreeterImpl implements Greeter {
	
	private static final String DEFAULT_SENTENCE = "DEFAULT";
	
	private String name = "";
	private String surname = "";
	public void setName(String name) {
		this.name = name;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getRegards(int number) {
		String ret = "";
		
		switch (number) {
		case 1:
			ret = "Sentence one";
			break;
		case 2:
			ret = "Sentence two";
			break;
		case 3:
			ret = "Sentence three";
			break;
		case 4:
			ret = "Sentence four";
			break;

		default:
			ret = DEFAULT_SENTENCE;
			break;
		}
		
		return this.name + " - " + this.surname + " :::::::: " + ret;
	}

}
